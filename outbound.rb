#!/usr/bin/env ruby

# Copyright (C) 2014  Giménez Silva Germán Alberto
# ggerman@gmail.com
# http://ggermanblog.bestwebdesigncompany.info/

=begin
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=end    

require 'anemone'
require 'nokogiri'
require 'uri'
require 'open-uri'
require 'open_uri_redirections'

class Outbound
  attr_accessor :body, :hostname
  
  def resume (url)
    
    unless url.empty?
      @hostname = URI(url)
      file = File.open "log_#{@hostname.host.to_s}.html", "a"
      
      Anemone.crawl(url) do |anemone|
	anemone.on_every_page do |page|
	  file.puts "<h1>#{page.url}</h1>"
	  print 'o'
	  if page_url_include_hostname?(page, @hostname)
		begin
		  file.puts "<ul>"
		  @body = Nokogiri::HTML(open(page.url, :allow_redirections => :safe))
		      @body.css("body a").map do |link|
			unless page_url_no_include_hostname?(link, hostname)
			  if is_external_link?(link)
			    file.puts "<li>#{link['href']}</li>"
			    print '.'
			  end
			end
		  end
		rescue OpenURI::HTTPError => error
		  response = error.io
		  file.puts "<li><span style=\"color: #ff0000\">#{page.url} - #{response.status}</span></li>"
		  print 'X'
		end
		file.puts "</ul>"
	  end
	end
      end
      
    end
    
  end
  
  
  def page_url_no_include_hostname?(link, hostname)
    link['href'].to_s.include? @hostname.host.to_s
  end
 
  def is_external_link?(link)
    link['href'].to_s.include? "http://" or link['href'].to_s.include? "https://"
  end
  
  def page_url_include_hostname?(page, hostname)
    page.url.to_s.include? @hostname.host.to_s and page.url.to_s.include? "http://"
  end
  
end

link = Outbound.new
link.resume ARGV[0]
